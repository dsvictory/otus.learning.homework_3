﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;
        private readonly ILogger<EfDbInitializer> _logger;

        public EfDbInitializer(DataContext dataContext, ILogger<EfDbInitializer> logger)
        {
            _dataContext = dataContext;
            _logger = logger;
        }
        
        public void InitializeDb()
        {
            var m = _dataContext.Database.GetPendingMigrations();

            _logger.LogInformation(_dataContext.Database.GetDbConnection().ConnectionString);

            if (m.Any()) {
                _dataContext.Database.Migrate();
                _logger.LogInformation("Migration was done!");
            }

            // Проверка присутствия данных по контрольной таблице
            if (!_dataContext.Employees.Any()) {

                _logger.LogInformation("Seeding is starting...");

                _dataContext.AddRange(FakeDataFactory.Employees);
                _dataContext.SaveChanges();

                _dataContext.AddRange(FakeDataFactory.Preferences);
                _dataContext.SaveChanges();

                _dataContext.AddRange(FakeDataFactory.Customers);
                _dataContext.SaveChanges();
            }
            
        }
    }
}